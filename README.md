CoreMedia Blueprint
=======================

This is the public repository for the CoreMedia Blueprints Workspace, it contains branches for each AEP and tags for each release.

### CoreMedia Content Cloud Releases

The releases for the current product are placed on branches named `cmcc-10-<AEP Version>` and are tagged `cmcc-10-<AEP Version>.<AMP Version>`. Each new AEP will be branched from the latest AMP to make upgrades as easy as possible. 

```
                      1907.1  1907.3
cmcc-10-1907 :--------|-------|------------ HEAD
                              \
cmcc-10-1910                    `---------- HEAD                    
```

* [cmcc-10-1910](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cmcc-10-1910)
* [cmcc-10-1907](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cmcc-10-1907)


## Previous Releases

### CoreMedia CMS 9 and LiveContext Releases

The releases for CMS 9 and LiveContext are split up into 4 different branches named `<Product>-<AEP Version>` and tagged `<Product>-<AEP Version>.<AMP Version>`.

#### CMS-9

* [cms-9-1904](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cms-9-1904)
* [cms-9-1901](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cms-9-1901)
* [cms-9-1810](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cms-9-1810)
* [cms-9-1807](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cms-9-1807)
* [cms-9-1804](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cms-9-1804)
* [cms-9-1801](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cms-9-1801)
* [cms-9-1710](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cms-9-1710)
* [cms-9-1707](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cms-9-1707)
* [cms-9-1704](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/cms-9-1704)

#### LiveContext for IBM WCS

* [livecontext-3-for-ibm-wcs-1904](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-ibm-wcs-1904)
* [livecontext-3-for-ibm-wcs-1901](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-ibm-wcs-1901)
* [livecontext-3-for-ibm-wcs-1810](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-ibm-wcs-1810)
* [livecontext-3-for-ibm-wcs-1807](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-ibm-wcs-1807)
* [livecontext-3-for-ibm-wcs-1804](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-ibm-wcs-1804)
* [livecontext-3-for-ibm-wcs-1801](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-ibm-wcs-1801)
* [livecontext-3-for-ibm-wcs-1710](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-ibm-wcs-1710)
* [livecontext-3-for-ibm-wcs-1707](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-ibm-wcs-1707)
* [livecontext-3-for-ibm-wcs-1701](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-ibm-wcs-1701)
* [livecontext-3-for-ibm-wcs-1604](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-ibm-wcs-1604)

#### LiveContext for SAP Hybris

* [livecontext-3-for-sap-hybris-1904](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-sap-hybris-1904)
* [livecontext-3-for-sap-hybris-1901](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-sap-hybris-1901)
* [livecontext-3-for-sap-hybris-1810](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-sap-hybris-1810)
* [livecontext-3-for-sap-hybris-1807](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-sap-hybris-1807)
* [livecontext-3-for-sap-hybris-1804](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-sap-hybris-1804)
* [livecontext-3-for-sap-hybris-1801](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-sap-hybris-1801)
* [livecontext-3-for-sap-hybris-1710](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-sap-hybris-1710)
* [livecontext-3-for-sap-hybris-1707](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-sap-hybris-1707)

#### LiveContext for Salesforce Commerce Cloud

* [livecontext-3-for-salesforce-commerce-cloud-1904](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-salesforce-commerce-cloud-1904)
* [livecontext-3-for-salesforce-commerce-cloud-1901](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-salesforce-commerce-cloud-1901)
* [livecontext-3-for-salesforce-commerce-cloud-1810](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-salesforce-commerce-cloud-1810)
* [livecontext-3-for-salesforce-commerce-cloud-1807](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-salesforce-commerce-cloud-1807)
* [livecontext-3-for-salesforce-commerce-cloud-1804](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-salesforce-commerce-cloud-1804)
* [livecontext-3-for-salesforce-commerce-cloud-1801](https://github.com/coremedia-contributions/coremedia-blueprints-workspace/tree/livecontext-3-for-salesforce-commerce-cloud-1801)
